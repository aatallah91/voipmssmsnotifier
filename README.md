[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/voipmssmsnotifier.tgharib)

A messaging app for VoIP.ms that notifies you when you receive a SMS text message

Steps to use the program:
1. Go to https://voip.ms/m/api.php. Enable the API. Set the enabled IP addresses to 0.0.0.0. Set an API password.
2. Run the app once. It will immediately close.
3. Make a text file at `~/.config/voipmssmsnotifier.tgharib/settings.ini` where the first line is your VoIP.ms username and the second line is your VoIP.ms API password and the third line is the polling interval time (the app waits X seconds between polls).
4. You should be able to use the program now. `~/.config/voipmssmsnotifier.tgharib/last_run.log` contains the logs of the last app instance. As long as the app is running, you will get notifications when you receive a SMS text message.

Sample settings.ini:
```
email@domain.com
MyPassword
300
```

openSUSE packages needed for development: libdbus-c++-1-1 and libdbus-c++-devel and libopenssl-devel