extern crate reqwest;
extern crate xdg;
#[macro_use]
extern crate serde_derive;
extern crate chrono;
extern crate dbus;
extern crate serde_json;
#[macro_use]
extern crate log;
extern crate log4rs;
extern crate log_panics;

macro_rules! APP_NAME {
    () => {
        "voipmssmsnotifier"
    };
}
macro_rules! AUTHOR {
    () => {
        "tgharib"
    };
}
macro_rules! FULL_APP_NAME {
    () => {
        concat!(APP_NAME!(), ".", AUTHOR!())
    };
}
macro_rules! FULL_APP_NAME_URL_ENCODED {
    () => {
        concat!(APP_NAME!(), "_2e", AUTHOR!())
    };
}
macro_rules! EXTENDED_APP_NAME {
    () => {
        concat!(APP_NAME!(), ".", AUTHOR!(), "_", APP_NAME!())
    };
}

use dbus::blocking::{Connection, Proxy};
use log::LevelFilter;
use log4rs::append::file::FileAppender;
use log4rs::config::{Appender, Config, Root};
use log4rs::encode::pattern::PatternEncoder;
use std::fs::{File, OpenOptions};
use std::io::{ErrorKind, Read, Seek, Write};
use std::path::PathBuf;

#[derive(Deserialize, Debug)]
struct SMS {
    id: String,
    date: String,
    r#type: String,
    did: String,
    contact: String,
    message: String,
}

#[derive(Deserialize, Debug)]
struct GetSMSResponse {
    status: String,
    sms: Vec<SMS>,
}

const FREEDESKTOP_BUS_NAME: &str = "org.freedesktop.Notifications";
const FREEDESKTOP_OBJECT_PATH: &str = "/org/freedesktop/Notifications";
const FREEDESKTOP_INTERFACE: &str = "org.freedesktop.Notifications";
const FREEDESKTOP_METHOD: &str = "Notify";

const POSTAL_BUS_NAME: &str = "com.ubuntu.Postal";
const POSTAL_OBJECT_PATH: &str = concat!("/com/ubuntu/Postal/", FULL_APP_NAME_URL_ENCODED!());
const POSTAL_INTERFACE: &str = "com.ubuntu.Postal";
const POSTAL_METHOD: &str = "Post";

const COMPILE_FOR_UBPORTS: bool = false;

fn send_notification(
    notification_proxy: &Proxy<'_, &'_ Connection>,
    message: &str,
    vibrate: bool,
    sound: bool,
    persist: bool,
) {
    if COMPILE_FOR_UBPORTS {
        let (): () = notification_proxy.method_call(POSTAL_INTERFACE,
        POSTAL_METHOD,
        (EXTENDED_APP_NAME!(),
        format!("{{\"notification\":{{\"card\":{{\"icon\":\"/opt/click.ubuntu.com/voipmssmsnotifier.tgharib/current/logo.svg\",\"summary\":\"VoIP.msSMSNotifier\",\"body\":\"{message}\",\"popup\":true,\"persist\":{persist}}},\"vibrate\":{vibrate},\"sound\":{sound}}}}}", message=message, vibrate=vibrate, sound=sound, persist=persist))
        ).unwrap();
    } else {
        let (): () = notification_proxy
            .method_call(
                FREEDESKTOP_INTERFACE,
                FREEDESKTOP_METHOD,
                (
                    EXTENDED_APP_NAME!(),   // app_name
                    0u32,                   // replaces_id
                    "mail-send-receive",    // app_icon
                    "VoIP.ms SMS Notifier", // summary
                    message,                // body
                    vec![""],               // actions
                    std::collections::HashMap::<String, dbus::arg::Variant<Box<dyn dbus::arg::RefArg>>>::new(), // hints
                    10000i32,               // timeout
                ),
            )
            .unwrap();
    }
}

fn create_notification_proxy(session_connection: &Connection) -> Proxy<'_, &'_ Connection> {
    if COMPILE_FOR_UBPORTS {
        session_connection.with_proxy(POSTAL_BUS_NAME, POSTAL_OBJECT_PATH, std::time::Duration::from_secs(5))
    } else {
        session_connection.with_proxy(
            FREEDESKTOP_BUS_NAME,
            FREEDESKTOP_OBJECT_PATH,
            std::time::Duration::from_secs(5),
        )
    }
}

fn read_settings_file(settings_path: &PathBuf) -> (String, String, u64) {
    let mut account_data_file = OpenOptions::new()
        .read(true)
        .open(settings_path)
        .expect("Unable to open settings.ini");
    let mut account_data = String::new();
    account_data_file
        .read_to_string(&mut account_data)
        .expect("Unable to read settings.ini");
    let mut account_data_lines = account_data.lines();
    let username = account_data_lines.next().unwrap().to_owned();
    let password = account_data_lines.next().unwrap().to_owned();
    let sleep_time_seconds = account_data_lines.next().unwrap().to_owned().parse::<u64>().unwrap();

    (username, password, sleep_time_seconds)
}

fn send_http_post(client: &reqwest::Client, username: &String, password: &String) -> Result<String, reqwest::Error> {
    let request_url = "https://voip.ms/api/v1/rest.php";
    let form = reqwest::multipart::Form::new()
        .text("api_username", username.to_owned())
        .text("api_password", password.to_owned())
        .text("method", "getSMS")
        .text("from", "2019-11-01")
        .text("type", "1")
        .text("limit", "1");

    let mut response = client.post(request_url).multipart(form).send()?;

    let mut response_text = String::new();
    response
        .read_to_string(&mut response_text)
        .expect("Unable to copy response body into string");
    info!("http response text: {}", response_text);
    Ok(response_text)
}

fn get_latest_sms_date_time(
    client: &reqwest::Client,
    username: &String,
    password: &String,
) -> Result<chrono::NaiveDateTime, reqwest::Error> {
    let http_response_text = send_http_post(&client, username, password)?;
    let response_body_struct: GetSMSResponse =
        serde_json::from_str(&http_response_text).expect("Unable to deserialize response body string into struct");
    let latest_sms_date_time_string = &response_body_struct.sms[0].date;
    info!("Date of latest SMS in string format: {}", latest_sms_date_time_string);

    let latest_sms_date_time = chrono::NaiveDateTime::parse_from_str(latest_sms_date_time_string, "%Y-%m-%d %H:%M:%S")
        .expect("Unable to parse date from SMS API");
    info!("Date of latest SMS after parse: {}", latest_sms_date_time);

    Ok(latest_sms_date_time)
}

fn get_last_sms_date_time(
    last_sms_date_time_file: &mut File,
    last_sms_date_time_path: &PathBuf,
) -> chrono::NaiveDateTime {
    let mut last_sms_date_time_data = String::new();
    last_sms_date_time_file
        .read_to_string(&mut last_sms_date_time_data)
        .expect("Unable to read last_sms_date_time.txt");
    let mut last_sms_date_time_data_lines = last_sms_date_time_data.lines();

    chrono::NaiveDateTime::parse_from_str(&last_sms_date_time_data_lines.next().unwrap(), "%Y-%m-%d %H:%M:%S")
        .unwrap_or_else(|error| {
            std::fs::remove_file(last_sms_date_time_path)
                .expect("Unable to delete last_sms_date_time.txt after failed parse");
            panic!(
                "Unable to parse date from last_sms_date_time.txt so file was deleted: {:?}",
                error
            );
        })
}

fn compare_last_and_latest_sms(
    file: &mut File,
    notification_proxy: &Proxy<'_, &'_ Connection>,
    latest_sms_date_time: &chrono::NaiveDateTime,
    last_sms_date_time: &chrono::NaiveDateTime,
) {
    if latest_sms_date_time > last_sms_date_time {
        info!("New text message received!");
        send_notification(&notification_proxy, "New text message received!", true, true, true);

        file.seek(std::io::SeekFrom::Start(0))
            .expect("Unable to seek to start of last_sms_date_time.txt for overwriting");
        file.write_all(latest_sms_date_time.to_string().as_bytes())
            .expect("Unable to write data to last_sms_date_time.txt");

        file.set_len(latest_sms_date_time.to_string().chars().count() as u64)
            .expect("Unable to truncate remainder bytes in last_sms_date_time.txt");
    // TODO: is the above cast good?
    } else {
        info!("No new text messages.");
    }
}

fn get_xdg_paths() -> (PathBuf, PathBuf, PathBuf) {
    let xdg_dirs = xdg::BaseDirectories::with_prefix(FULL_APP_NAME!()).unwrap();
    let last_sms_date_time_path = xdg_dirs
        .place_config_file("last_sms_date_time.txt")
        .expect("Unable to create configuration directory (XDG Base Directory Specification)");
    let settings_path = xdg_dirs
        .place_config_file("settings.ini")
        .expect("Unable to create configuration directory (XDG Base Directory Specification)");
    let last_run_path = xdg_dirs
        .place_config_file("last_run.log")
        .expect("Unable to create configuration directory (XDG Base Directory Specification)");

    (last_sms_date_time_path, settings_path, last_run_path)
}

fn initialize_logging(last_run_path: &PathBuf) {
    if std::path::Path::new(&last_run_path).exists() {
        std::fs::remove_file(&last_run_path).expect("Unable to delete last_run.log");
    }
    log_panics::init();
    let log_to_file = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new("{d(%Y-%m-%d %H:%M:%S)} [{l}] {m}{n}"))) // date, log level, log message, new line
        .build(&last_run_path)
        .unwrap();
    let config = Config::builder()
        .appender(Appender::builder().build("log_to_file", Box::new(log_to_file)))
        .build(Root::builder().appender("log_to_file").build(LevelFilter::Info))
        .unwrap();

    log4rs::init_config(config).unwrap();
    info!("Booting up v{}", env!("CARGO_PKG_VERSION"));
}

fn main() {
    let (last_sms_date_time_path, settings_path, last_run_path) = get_xdg_paths();
    initialize_logging(&last_run_path);

    let session_connection = Connection::new_session().unwrap();
    let notification_proxy = create_notification_proxy(&session_connection);
    let (username, password, sleep_time_seconds) = read_settings_file(&settings_path);

    loop {
        info!("Checking for new text messages...");
        let client = reqwest::Client::new();
        let latest_sms_date_time = get_latest_sms_date_time(&client, &username, &password);
        let latest_sms_date_time = match latest_sms_date_time {
            Ok(latest_sms_date_time) => latest_sms_date_time,
            Err(error) => {
                if error.url().unwrap().as_str() == "https://voip.ms/api/v1/rest.php" {
                    info!(
                        "Encountered network issue so going to sleep until next loop: {:?}",
                        error
                    );
                    std::thread::sleep(std::time::Duration::from_secs(sleep_time_seconds));
                    continue;
                } else {
                    panic!("Unable to get latest sms date time: {:?}", error)
                }
            }
        };

        let mut last_sms_date_time_file = OpenOptions::new()
            .write(true)
            .read(true)
            .open(&last_sms_date_time_path)
            .unwrap_or_else(|error| {
                if error.kind() == ErrorKind::NotFound {
                    info!("last_sms_date_time.txt not found so creating a new last_sms_date_time.txt");
                    let mut f2 =
                        File::create(&last_sms_date_time_path).expect("Unable to create last_sms_date_time.txt");
                    f2.write_all(latest_sms_date_time.to_string().as_bytes())
                        .expect("Unable to write data to last_sms_date_time.txt");
                    panic!("Wrote data to last_sms_date_time.txt"); // TODO: return main without panicking
                } else {
                    panic!("Unable to open last_sms_date_time.txt: {:?}", error);
                }
            });

        let last_sms_date_time = get_last_sms_date_time(&mut last_sms_date_time_file, &last_sms_date_time_path);

        compare_last_and_latest_sms(
            &mut last_sms_date_time_file,
            &notification_proxy,
            &latest_sms_date_time,
            &last_sms_date_time,
        );

        std::thread::sleep(std::time::Duration::from_secs(sleep_time_seconds));
    }
}
