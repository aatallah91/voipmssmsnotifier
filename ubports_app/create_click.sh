#!/bin/bash
export CARGO_TARGET_DIR=../voipmssmsnotifier-target
rm *.click
cd ..
cargo build --release
cd ubports_app
mkdir package
cp files/* package/
cp ../../voipmssmsnotifier-target/release/voipmssmsnotifier package/voipmssmsnotifier
click build package
# install with pkcon install-local --allow-untrusted name.click
